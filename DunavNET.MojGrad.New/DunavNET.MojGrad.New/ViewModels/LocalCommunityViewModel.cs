﻿using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Models;
using DunavNET.MojGrad.New.Services;
using DunavNET.MojGrad.New.Tools;
using DunavNET.MojGrad.New.Views.WeLive;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace DunavNET.MojGrad.New.ViewModels
{
    public class LocalCommunityViewModel : BaseViewModel
    {
        public LocalCommunityViewModel(INavigation navigation, Map map)
        {
            Communities = new ObservableCollection<LocalCommunityModel>();
            Topics = new ObservableCollection<TopicModel>();
            Posts = new ObservableCollection<LocalCommunityPostModel>();

            RefreshPinsCommand = new Command(() => RefreshPins());

            _Map = map;
            _Navigation = navigation;

            InitData(map);
        }

        private Map _Map;
        private INavigation _Navigation;

        private double _Lat;
        private double _Lng;

        private ObservableCollection<TopicModel> _Topics;
        public ObservableCollection<TopicModel> Topics
        {
            get => _Topics;
            set => SetProperty(ref _Topics, value);
        }

        private int _SelectedTopic;
        public int SelectedTopic
        {
            get => _SelectedTopic;
            set => SetProperty(ref _SelectedTopic, value);
        }

        private string _TopicName;
        public string TopicName
        {
            get => _TopicName;
            set => SetProperty(ref _TopicName, value);
        }

        private ObservableCollection<LocalCommunityModel> _Communities;
        public ObservableCollection<LocalCommunityModel> Communities
        {
            get => _Communities;
            set => SetProperty(ref _Communities, value);
        }

        private int _SelectedCommunity;
        public int SelectedCommunity
        {
            get => _SelectedCommunity;
            set => SetProperty(ref _SelectedCommunity, value);
        }

        private string _CommunityName;
        public string CommunityName
        {
            get => _CommunityName;
            set => SetProperty(ref _CommunityName, value);
        }


        private ObservableCollection<LocalCommunityPostModel> _Posts;
        public ObservableCollection<LocalCommunityPostModel> Posts
        {
            get => _Posts;
            set => SetProperty(ref _Posts, value);
        }

        public Command RefreshPinsCommand { get; }

        private async void InitData(Map map)
        {
            ShowDialog();

            Communities = await DataService.GetLocalCommunities();
            Topics = await DataService.GetLocalCommunityTopics();
            Posts = await DataService.GetCommunityPosts();
            
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        await App.Current.MainPage.DisplayAlert(LocalizationResource.Info, "Potreban je pristup lokaciji.", LocalizationResource.Ok);
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Location))
                        status = results[Permission.Location];
                }
                if (status == PermissionStatus.Granted)
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 5000;

                    if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        var position = await locator.GetLastKnownLocationAsync();
                        _Lat = position.Latitude;
                        _Lng = position.Longitude;
                    }
                    else
                    {
                        _Lat = LocalData.GetLocalLat();
                        _Lng = LocalData.GetLocalLng();
                    }
                }
                else
                {
                    _Lat = LocalData.GetLocalLat();
                    _Lng = LocalData.GetLocalLng();
                }

            }
            catch (Exception ex)
            {
                _Lat = DefaultValueConstants.DEFAULT_LATITUDE;
                _Lng = DefaultValueConstants.DEFAULT_LONGITUDE;
            }
                

            Topics.Insert(0, new TopicModel { TopicId = 0, Name = LocalizationResource.All });
            Communities.Insert(0, new LocalCommunityModel { ID = 0, Name = LocalizationResource.All, Address = "", Lat = 0, Lng = 0});

            SelectedTopic = 0;
            SelectedCommunity = (int) Utils.GetClosestLocalCommunity(Communities.ToList(), new Position(_Lat, _Lng));

            TopicName = Topics.ElementAt(SelectedTopic).Name;
            CommunityName = Communities.ElementAt(SelectedCommunity).Name;

            InitMap(map);

            HideDialog();
        }

        private void InitMap(Map map)
        {
            var center = Utils.GetCentroid(Posts.ToList());
            double distance = Utils.FurthestDistance(Posts.ToList());

            long communityId = Communities.ElementAt(SelectedCommunity).ID;

            map.MoveToRegion(MapSpan.FromCenterAndRadius(center, Distance.FromKilometers(distance + 1)));
           
            var pins = Posts.Where(x => x.CommunityId == communityId).Select(x => new Pin
            {
                Position = new Position(x.Location.Latitude, x.Location.Longitude),
                Label = x.Title,
                Address = x.Location.AddressLine1,
                Type = PinType.Place,
                BindingContext = x
            }).ToList();

            foreach (var pin in pins)
            {
                pin.Clicked += Item_Clicked;
                map.Pins.Add(pin);
            }
        }

        private void RefreshPins()
        {
            long topicId = Topics.ElementAt(SelectedTopic).TopicId;
            long communityId = Communities.ElementAt(SelectedCommunity).ID;

            var pins = Posts.Where(x => (topicId == 0 ? true : x.TopicID == topicId) && (communityId == 0 ? true : x.CommunityId == communityId)).Select(x => 
            new Pin
            {
                Position = new Position(x.Location.Latitude, x.Location.Longitude),
                Label = x.Title,
                Address = x.Location.AddressLine1,
                Type = PinType.Place,
                BindingContext = x
            }).ToList();

            _Map.Pins.Clear();

            foreach (var pin in pins)
            {
                pin.Clicked += Item_Clicked;
                _Map.Pins.Add(pin);
            }

            DataService.PostLogWeLive("app personalized", "AppPersonalized");
        }

        private void Item_Clicked(object sender, EventArgs e)
        {
            var p = sender as Pin;
            var selectedItem = p.BindingContext as LocalCommunityPostModel;
            _Navigation.PushAsync(new PostDetailsPage(selectedItem));
        }
    }
}
