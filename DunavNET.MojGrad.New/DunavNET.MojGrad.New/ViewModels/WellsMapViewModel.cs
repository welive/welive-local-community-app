﻿using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Models;
using DunavNET.MojGrad.New.Services;
using DunavNET.MojGrad.New.Tools;

using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

using System;
using System.Linq;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using DunavNET.MojGrad.New.Views.WeLive;

namespace DunavNET.MojGrad.New.ViewModels
{
    public class WellsMapViewModel : BaseViewModel
    {
        public WellsMapViewModel(INavigation navigation, Map map)
        {
            Wells = new ObservableCollection<WellDataModel>();
            _Map = map;
            _Navigation = navigation;
            _InitData(map);

        }

        private Map _Map;
        private INavigation _Navigation;
        private double _Lat;
        private double _Lng;

        private ObservableCollection<WellDataModel> _Wells;
        public ObservableCollection<WellDataModel> Wells
        {
            get => _Wells;
            set => SetProperty(ref _Wells, value);
        }

        private async void _InitData(Map map)
        {
            ShowDialog();
            Wells = await DataService.GetWellDataAsync();

            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        await App.Current.MainPage.DisplayAlert(LocalizationResource.Info, "Potreban je pristup lokaciji.", LocalizationResource.Ok);
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Location))
                        status = results[Permission.Location];
                }
                if (status == PermissionStatus.Granted)
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 5000;

                    if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        var position = await locator.GetLastKnownLocationAsync();
                        _Lat = position.Latitude;
                        _Lng = position.Longitude;
                    }
                    else
                    {
                        _Lat = LocalData.GetLocalLat();
                        _Lng = LocalData.GetLocalLng();
                    }
                }
                else
                {
                    _Lat = LocalData.GetLocalLat();
                    _Lng = LocalData.GetLocalLng();
                }

            }
            catch (Exception ex)
            {
                _Lat = DefaultValueConstants.DEFAULT_LATITUDE;
                _Lng = DefaultValueConstants.DEFAULT_LONGITUDE;
            }

            var center = Utils.GetCentroid(Wells.ToList());
            double distance = Utils.FurthestDistance(Wells.ToList());
            map.MoveToRegion(MapSpan.FromCenterAndRadius(center, Distance.FromKilometers(distance + 1)));

            var pins = Wells.Select(x =>
            new Pin
            {
                Position = new Position(x.GeoX.Value, x.GeoY.Value),
                Label = x.Name,
                Address = x.Address,
                Type = PinType.Place,
                BindingContext = x
            }).ToList();

            foreach (var pin in pins)
            {
                pin.Clicked += _PinClicked;
                map.Pins.Add(pin);
            }

            HideDialog();
        }

        private void _PinClicked(object sender, EventArgs e)
        {
            var p = sender as Pin;
            var selected = p.BindingContext as WellDataModel;
            _Navigation.PushAsync(new WellDetailsPage(selected));
        }
    }
}
