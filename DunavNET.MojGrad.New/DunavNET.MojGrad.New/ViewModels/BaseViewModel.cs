﻿using Acr.UserDialogs;
using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DunavNET.MojGrad.New.ViewModels
{
    public class BaseViewModel : ObservableObject
    {
        protected BaseViewModel()
        {
        }


        protected IProgressDialog Dialogs { get; set; }

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        protected void ShowDialog(int? i = null)
        {
                          
            if (this.Dialogs == null)
                this.Dialogs = UserDialogs.Instance.Loading(LocalizationResource.Loading);
            this.Dialogs.Show();
            
        }

        protected void HideDialog(int? i = null)
        {            
            if (this.Dialogs == null)
                this.Dialogs = UserDialogs.Instance.Loading(LocalizationResource.Loading);
            this.Dialogs.Hide();
            
        }

        public virtual void OnPageApearing() { }
    }
}
