﻿using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Models;
using DunavNET.MojGrad.New.Services;
using DunavNET.MojGrad.New.Tools;
using DunavNET.MojGrad.New.Views.Authentication;
using DunavNET.MojGrad.New.Views.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Auth;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private INavigation Navigation;
        private Account account;
        private AccountStore store;

        public Command GoogleCommand { get;}
        public Command FacebookCommand { get; }
        public Command MicrosoftCommand { get;}
        public Command GuestCommand { get;}
        public Command WeLiveCommand { get; }

        public LoginViewModel(INavigation navigation)
        {
            store = AccountStore.Create();
            account = store.FindAccountsForService(XamarinAuth.AppName).FirstOrDefault();

            GoogleCommand = new Command(() => GoogleLogin());
            //FacebookCommand = new Command(() => Login(LoginProviders.FACEBOOK));
            //MicrosoftCommand = new Command(() => Login(LoginProviders.MICROSOFT));
            WeLiveCommand = new Command(() => Login(LoginProviders.WELIVE));
            GuestCommand = new Command(() => Guest());

            Navigation = navigation;
        }

        private async void Login(string provider)
        {
            await Navigation.PushAsync(new ProviderWebViewPage(provider));
        }

        private void GoogleLogin()
        {
            
            string clientId = null;
            string redirectUri = null;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    clientId = XamarinAuth.iOSClientId;
                    redirectUri = XamarinAuth.iOSRedirectUrl;
                    break;

                case Device.Android:
                    clientId = XamarinAuth.AndroidClientId;
                    redirectUri = XamarinAuth.AndroidRedirectUrl;
                    break;
            }

            var authenticator = new OAuth2Authenticator(
                clientId,
                null,
                XamarinAuth.Scope,
                new Uri(XamarinAuth.AuthorizeUrl),
                new Uri(redirectUri),
                new Uri(XamarinAuth.AccessTokenUrl),
                null,
                true);

            authenticator.ClearCookiesBeforeLogin = true;
            authenticator.Completed += OnAuthCompleted;
            authenticator.Error += OnAuthError;
            authenticator.BrowsingCompleted += Authenticator_BrowsingCompleted;

            AuthenticationState.Authenticator = authenticator;

            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(authenticator);           
        }

        private void Authenticator_BrowsingCompleted(object sender, EventArgs e)
        {
            HideDialog();
        }

        private void Guest()
        {
            App.Current.MainPage = new MyMasterDetailPage();
        }

        async void OnAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            ShowDialog();
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            if (e.IsAuthenticated)
            {
                // If the user is authenticated, request their basic user data from Google
                // UserInfoUrl = https://www.googleapis.com/oauth2/v2/userinfo
                var request = new OAuth2Request("GET", new Uri(XamarinAuth.UserInfoUrl), null, e.Account);
                string token = null;
                e.Account.Properties.TryGetValue("access_token", out token);

                if (!string.IsNullOrEmpty(token))
                {
                    string accessToken = await AuthenticationService.GoogleLogin(token);

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        UserInfoModel model = await AuthenticationService.GetUserInfo(accessToken);

                        LocalData.SaveTokenLocal(accessToken);
                        string email = model.Email;

                        LocalData.SaveEmailLocal(model.Email);
                        App.Current.MainPage = new MyMasterDetailPage();


                    }
                }

                if (account != null)
                {
                    store.Delete(account, XamarinAuth.AppName);
                }

                await store.SaveAsync(account = e.Account, XamarinAuth.AppName);
            }

            HideDialog();
        }

        private void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            App.Current.MainPage.DisplayAlert(LocalizationResource.Info, LocalizationResource.LoginError, LocalizationResource.Ok);

            HideDialog();
        }

        public override void OnPageApearing()
        {
            base.OnPageApearing();
            HideDialog();
        }
    }
}
