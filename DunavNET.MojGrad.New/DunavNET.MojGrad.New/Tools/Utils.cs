﻿using DunavNET.MojGrad.New.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace DunavNET.MojGrad.New.Tools
{
    public class Utils
    {

        public static Position GetCentroid(List<LocalCommunityPostModel> posts)
        {
            if (posts.Count == 1)
            {
                var model = posts.ElementAt(0);
                double x = model.Location.Latitude;
                double y = model.Location.Longitude;
                return new Position(x, y);
            }

            double accumulatedArea = 0;
            double centerX = 0;
            double centerY = 0;

            for (int i = 0, j = posts.Count - 1; i < posts.Count; j = i++)
            {
                var modelI = posts.ElementAt(i);
                var modelJ = posts.ElementAt(j);

                double xi = modelI.Location.Latitude;
                double yi = modelI.Location.Longitude;

                double xj = modelJ.Location.Latitude;
                double yj = modelJ.Location.Longitude;

                double temp = xi * yj - xj * yi;
                accumulatedArea += temp;
                centerX += (xi + xj) * temp;
                centerY += (yi + yj) * temp;
            }

            if (Math.Abs(accumulatedArea) < 1E-7f)
                return new Position();  // Avoid division by zero

            accumulatedArea *= 3f;
            return new Position(centerX / accumulatedArea, centerY / accumulatedArea);
        }

        public static Position GetCentroid(List<WellDataModel> posts)
        {
            if (posts.Count == 1)
            {
                var model = posts.ElementAt(0);
                double x = model.GeoX.Value;
                double y = model.GeoY.Value;
                return new Position(x, y);
            }

            double accumulatedArea = 0;
            double centerX = 0;
            double centerY = 0;

            for (int i = 0, j = posts.Count - 1; i < posts.Count; j = i++)
            {
                var modelI = posts.ElementAt(i);
                var modelJ = posts.ElementAt(j);

                double xi = modelI.GeoX.Value;
                double yi = modelI.GeoY.Value;

                double xj = modelJ.GeoX.Value;
                double yj = modelJ.GeoY.Value;

                double temp = xi * yj - xj * yi;
                accumulatedArea += temp;
                centerX += (xi + xj) * temp;
                centerY += (yi + yj) * temp;
            }

            if (Math.Abs(accumulatedArea) < 1E-7f)
                return new Position();  // Avoid division by zero

            accumulatedArea *= 3f;
            return new Position(centerX / accumulatedArea, centerY / accumulatedArea);
        }

        public static double FurthestDistance(List<LocalCommunityPostModel> posts)
        {
            double distance = 0;

            if (posts.Count > 1)
            {

                for (int i = 1; i < posts.Count; i++)
                {
                    var modelI = posts.ElementAt(i);
                    var modelJ = posts.ElementAt(i - 1);

                    double xi = modelI.Location.Latitude;
                    double yi = modelI.Location.Longitude;

                    double xj = modelJ.Location.Latitude;
                    double yj = modelJ.Location.Longitude;

                    double tempDist = GetDistanceBetweenTwoPoints(yi, xi, yj, xj);

                    if (tempDist > distance)
                        distance = tempDist;
                }
            }
            else
            {
                distance = 2;
            }

            return distance;
        }

        public static double FurthestDistance(List<WellDataModel> posts)
        {
            double distance = 0;

            if (posts.Count > 1)
            {

                for (int i = 1; i < posts.Count; i++)
                {
                    var modelI = posts.ElementAt(i);
                    var modelJ = posts.ElementAt(i - 1);

                    double xi = modelI.GeoX.Value;
                    double yi = modelI.GeoY.Value;

                    double xj = modelJ.GeoX.Value;
                    double yj = modelJ.GeoY.Value;

                    double tempDist = GetDistanceBetweenTwoPoints(yi, xi, yj, xj);

                    if (tempDist > distance)
                        distance = tempDist;
                }
            }
            else
            {
                distance = 2;
            }

            return distance;
        }

        public static long GetClosestLocalCommunity(List<LocalCommunityModel> communities, Position position)
        {
            double distance = 100000000000;
            int index = 0;

            for (int i = 0; i < communities.Count; i++)
            {
                var modelI = communities.ElementAt(i);

                double xi = (double) modelI.Lat;
                double yi = (double) modelI.Lng;

                double xj = position.Latitude;
                double yj = position.Longitude;

                double tempDist = GetDistanceBetweenTwoPoints(yi, xi, yj, xj);

                if (tempDist < distance)
                {
                    index = i;
                    distance = tempDist;
                }
            }
            return communities.ElementAt(index).ID; 
        }

        private static double GetDistanceBetweenTwoPoints(double lon1, double lat1, double lon2, double lat2)
        {
            double R = 6371; // km

            double sLat1 = Math.Sin(Radians(lat1));
            double sLat2 = Math.Sin(Radians(lat2));
            double cLat1 = Math.Cos(Radians(lat1));
            double cLat2 = Math.Cos(Radians(lat2));
            double cLon = Math.Cos(Radians(lon1) - Radians(lon2));

            double cosD = sLat1 * sLat2 + cLat1 * cLat2 * cLon;

            double d = Math.Acos(cosD);

            double dist = R * d;

            return dist;
        }

        private static double Radians(double x)
        {
            return (x * (Math.PI / 180));
        }
    }
}
