﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Tools
{
    public class LocalData
    {
        public static string GetLocalToken()
        {
            var properties = Application.Current.Properties.Any(x => x.Key == "token");
            string token = properties && Application.Current.Properties["token"] != null ? Application.Current.Properties["token"].ToString() : string.Empty;

            return token;
        }

        public static void SaveTokenLocal(string token)
        {
            Application.Current.Properties.Remove("token");
            Application.Current.Properties["token"] = token;
            var xx = Application.Current.Resources;
        }

        public static string GetLocalEmail()
        {
            var properties = Application.Current.Properties.Any(x => x.Key == "email");
            string token = properties && Application.Current.Properties["email"] != null ? Application.Current.Properties["email"].ToString() : string.Empty;

            return token;
        }

        public static void SaveEmailLocal(string token)
        {
            Application.Current.Properties["email"] = token;
        }

        public static string GetLocalAddress()
        {
            var properties = Application.Current.Properties.Any(x => x.Key == "address");
            string address = properties ? Application.Current.Properties["address"].ToString() : DefaultValueConstants.DEFAULT_ADDRESS;

            return address;
        }

        public static void SaveAddressLocal(string address)
        {
            Application.Current.Properties["address"] = address;
        }

        public static double GetLocalLat()
        {
            var properties = Application.Current.Properties.Any(x => x.Key == "latitude");
            double latitude = properties ? double.Parse(Application.Current.Properties["latitude"].ToString()) : DefaultValueConstants.DEFAULT_LATITUDE;

            return latitude;
        }

        public static void SaveLatLocal(double latitude)
        {
            Application.Current.Properties["latitude"] = latitude;
        }

        public static double GetLocalLng()
        {
            var properties = Application.Current.Properties.Any(x => x.Key == "longitude");
            double longitude = properties ? double.Parse(Application.Current.Properties["longitude"].ToString()) : DefaultValueConstants.DEFAULT_LONGITUDE;

            return longitude;
        }

        public static void SaveLngLocal(double longitude)
        {
            Application.Current.Properties["longitude"] = longitude;
        }

        public static string GetUID()
        {
            var properties = Application.Current.Properties.Any(x => x.Key == "UID");
            string uid = properties && Application.Current.Properties["UID"] != null ? Application.Current.Properties["UID"].ToString() : string.Empty;

            return uid;
        }

        public static void SaveUID(string uid)
        {
            Application.Current.Properties["UID"] = uid;
        }

        public static void ClearData()
        {
            Application.Current.Properties.Remove("token");
            Application.Current.Properties.Remove("email");
            Application.Current.Properties.Remove("UID");
        }
    }
}
