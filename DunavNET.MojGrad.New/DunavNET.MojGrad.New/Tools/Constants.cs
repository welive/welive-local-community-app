﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Tools
{
    public class AuthenticationConstants
    {        
        public static string GET_LOGIN_PROVIDERS = "https://cityimprovementnetapi.azurewebsites.net/api/Account/ExternalLogins?returnUrl=/&generateState=true";
        public static string BASE_API_URL = "https://cityimprovementnetapi.azurewebsites.net/";
        public static string GET_USER_INFO = "https://cityimprovementnetapi.azurewebsites.net/api/Account/UserInfo";       
        public static string REGISTER_USER = "https://cityimprovementnetapi.azurewebsites.net/api/Account/RegisterExternal";
        public static string GOOGLE_LOGIN(string token)
        {
            return string.Format("https://cityimprovementnetapi.azurewebsites.net/api/Account/GoogleLogin?token={0}", token);
        }
    }

    public class DefaultValueConstants
    {
        public static string DEFAULT_ADDRESS = "Bulevar Oslobođenja 81, Novi Sad";
        public static double DEFAULT_LATITUDE = 45.252587;
        public static double DEFAULT_LONGITUDE = 19.836889;
    }

    public class LoginProviders
    {
        public static readonly string GOOGLE = "Google";
        //public static readonly string FACEBOOK = "Facebook";
        //public static readonly string MICROSOFT = "Live";
        public static readonly string WELIVE = "WeLive";
    }

    public class MyColors
    {
        public static readonly Color PRIMARY = Color.FromHex("#8FA5AA");
        public static readonly Color ACCENT = Color.FromHex("#DC8B7F");
    }

    public class XamarinAuth
    {
        public static string AppName = "Local Community";

        // OAuth
        // For Google login, configure at https://console.developers.google.com/
        public static string iOSClientId = "<insert IOS client ID here>";
        public static string AndroidClientId = "274431512444-hvfp1sedjd79hu43fkf5e8hmsilgeqsj.apps.googleusercontent.com";

        // These values do not need changing
        public static string Scope = "https://www.googleapis.com/auth/userinfo.email";
        public static string AuthorizeUrl = "https://accounts.google.com/o/oauth2/auth";
        public static string AccessTokenUrl = "https://www.googleapis.com/oauth2/v4/token";
        public static string UserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";

        // Set these to reversed iOS/Android client ids, with :/oauth2redirect appended
        public static string iOSRedirectUrl = "<insert IOS redirect URL here>:/oauth2redirect";
        public static string AndroidRedirectUrl = "com.googleusercontent.apps.274431512444-hvfp1sedjd79hu43fkf5e8hmsilgeqsj:/oauth2redirect";
    }
}
