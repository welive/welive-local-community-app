﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Renderers
{
    public class CustomButton : Button
    {
        static BindableProperty CustomRadiusProperty = BindableProperty.CreateAttached("CustomRadius", typeof(int?), typeof(CustomButton), 0);
         

        public int? CustomRadius
        {
            get { return (int?)base.GetValue(CustomRadiusProperty); }
            set { base.SetValue(CustomRadiusProperty, value); }
        }
    }
}
