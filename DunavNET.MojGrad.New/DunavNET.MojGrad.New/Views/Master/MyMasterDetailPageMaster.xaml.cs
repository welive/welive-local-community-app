﻿using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DunavNET.MojGrad.New.Views.Master
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyMasterDetailPageMaster : ContentPage
    {
        public ListView ListView;

        public MyMasterDetailPageMaster()
        {
            InitializeComponent();

            BindingContext = new MyMasterDetailPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        class MyMasterDetailPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MyMasterDetailPageMenuItem> MenuItems { get; set; }

            public MyMasterDetailPageMasterViewModel()
            {
                if (!string.IsNullOrEmpty(LocalData.GetLocalToken()))
                {
                    MenuItems = new ObservableCollection<MyMasterDetailPageMenuItem>(new[]
                    {
                        new MyMasterDetailPageMenuItem { Id = 4, Title = LocalizationResource.LocalCommunity},
                        new MyMasterDetailPageMenuItem { Id = 6, Title = LocalizationResource.Wells},
                        new MyMasterDetailPageMenuItem { Id = 5, Title = LocalizationResource.Survey},
                        new MyMasterDetailPageMenuItem { Id = 2, Title = LocalizationResource.AboutApp },
                        new MyMasterDetailPageMenuItem { Id = 3, Title = LocalizationResource.LogOff },
                    });
                }
                else
                {

                    MenuItems = new ObservableCollection<MyMasterDetailPageMenuItem>(new[]
                    {
                        new MyMasterDetailPageMenuItem { Id = 4, Title = LocalizationResource.LocalCommunity},
                        new MyMasterDetailPageMenuItem { Id = 6, Title = LocalizationResource.Wells},
                        new MyMasterDetailPageMenuItem { Id = 5, Title = LocalizationResource.Survey},
                        new MyMasterDetailPageMenuItem { Id = 2, Title = LocalizationResource.AboutApp },
                        new MyMasterDetailPageMenuItem { Id = 3, Title = LocalizationResource.Login },
                    });
                }
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}