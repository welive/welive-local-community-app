﻿using DunavNET.MojGrad.MobileApp;
using DunavNET.MojGrad.New.Tools;
using DunavNET.MojGrad.New.Views.Authentication;
using DunavNET.MojGrad.New.Views.Shared;
using DunavNET.MojGrad.New.Views.WeLive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DunavNET.MojGrad.New.Views.Master
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyMasterDetailPage : MasterDetailPage
    {
        public MyMasterDetailPage()
        {
            InitializeComponent();


            Detail = new NavigationPage(new LocalCommunityPage());            
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
            Detail.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
            Detail.SetValue(NavigationPage.BarTextColorProperty, Color.White);

        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MyMasterDetailPageMenuItem;
            if (item == null)
                return;

            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

            switch (item.Id)
            {
                case 3:
                    DependencyService.Get<LogoutInterface>().DoOnLogout();
                    LocalData.ClearData();
                    LoginPage loginPage = new LoginPage();
                    App.Current.MainPage = new NavigationPage(loginPage);
                    NavigationPage.SetHasNavigationBar(loginPage, false);
                    App.Current.MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
                    App.Current.MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

                    break;
                case 2:
                    page = new AboutPage();
                    break;
                case 4:
                    page = new LocalCommunityPage();
                    break;
                case 5:
                    page = new SurvayWebView();
                    break;
                case 6:
                    page = new WellsMapPage();
                    break;
                default:
                    page = new WellsMapPage();
                    break;
            }

            if (item.Id != 3)
            {
                Detail = new NavigationPage(page);
                Detail.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
                Detail.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                IsPresented = false;

                MasterPage.ListView.SelectedItem = null;
            }
        }
    }
}