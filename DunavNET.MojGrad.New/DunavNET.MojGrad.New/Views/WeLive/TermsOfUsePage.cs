﻿using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Services;
using DunavNET.MojGrad.New.Tools;
using DunavNET.MojGrad.New.Views.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Views.WeLive
{
    public class TermsOfUsePage : ContentPage
    {
        public TermsOfUsePage()
        {
            Title = LocalizationResource.Title;
            Padding = new Thickness(30, 20, 30, 0);

            Label title = new Label { FontSize = 25, HorizontalOptions = LayoutOptions.Center, Text = LocalizationResource.TermsOfUse, HeightRequest = 100 };

            Label l1  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL1,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l2  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL2,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l3  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL3,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l4  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL4,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l5  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL5,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l6  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL6,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l7  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL7,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l8  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL8,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l9  = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL9,  HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l10 = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL10, HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };
            Label l11 = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.TermsL11, HorizontalTextAlignment = TextAlignment.Start, VerticalOptions = LayoutOptions.FillAndExpand };

            var btn = new Button { Text = LocalizationResource.Accept, BackgroundColor = MyColors.ACCENT, TextColor = Color.White, HorizontalOptions = LayoutOptions.Center };
            btn.Clicked += Btn_Clicked;

            var stack = new StackLayout
            {
                Children =
                {
                    title,
                    l1,
                    l2,
                    l3,
                    l4,
                    l5,
                    l6,
                    l7,
                    l8,
                    l9,
                    l10,
                    l11,
                    btn
                }
            };

            var scroll = new ScrollView { Content = stack };

            Content = scroll;

        }

        private void Btn_Clicked(object sender, EventArgs e)
        {
            DataService.SaveFlag();
            LoginPage loginPage = new LoginPage();
            App.Current.MainPage = new NavigationPage(loginPage);
            NavigationPage.SetHasNavigationBar(loginPage, false);
            App.Current.MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
            App.Current.MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);

        }
    }
}