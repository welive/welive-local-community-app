﻿using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Models;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Views.WeLive
{
    public class WellDetailsPage : ContentPage
    {
        public WellDetailsPage(WellDataModel model)
        {
            Title = LocalizationResource.WellsTitle;
            Padding = new Thickness(30, 20, 30, 0);

            var title = new Label { FontSize = 20, HorizontalOptions = LayoutOptions.Center, Text = model.Name, HeightRequest = 70 };
            var address = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = model.Address, HorizontalTextAlignment = TextAlignment.Start };
            var text = new Label { FontSize = 16, HorizontalOptions = LayoutOptions.FillAndExpand, Text = model.JoinedName, HorizontalTextAlignment = TextAlignment.Start };

            Content = new StackLayout
            {
                Children =
                {
                    title,
                    address,
                    text
                }
            };
        }
    }
}