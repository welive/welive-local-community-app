﻿using DunavNET.MojGrad.New.Models;
using Xamarin.Forms;
using DunavNET.MojGrad.New.AppResources;

namespace DunavNET.MojGrad.New.Views.WeLive
{
    public class PostDetailsPage : ContentPage
    {
        public PostDetailsPage(LocalCommunityPostModel postModel)
        {
            Title = LocalizationResource.PostDetails;
            Padding = new Thickness(30, 20, 30, 0);

            var title = new Label { FontSize = 20, HorizontalOptions = LayoutOptions.Center, Text = postModel.Title, HeightRequest = 70 };
            var address = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = postModel.Location.AddressLine1, HorizontalTextAlignment = TextAlignment.Start };
            var text = new Label { FontSize = 16, HorizontalOptions = LayoutOptions.FillAndExpand, Text = postModel.Description, HorizontalTextAlignment = TextAlignment.Start };

            Content = new ScrollView
            {
                Content = new StackLayout
                {
                    Children =
                    {
                        title,
                        address,
                        text
                    }
                }
            };
        }
    }
}