﻿using Acr.UserDialogs;
using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Views.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Views.WeLive
{
    public class SurvayWebView : ContentPage
    {
        private IProgressDialog dialog;
        private WebView webView;

        public SurvayWebView()
        {
            Title = LocalizationResource.Title;
            dialog = UserDialogs.Instance.Loading(LocalizationResource.Loading);
            dialog.Show();
        }

        protected override void OnAppearing()
        {
            string url = "https://in-app.cloudfoundry.welive.eu/html/index.html?app=localcommunity&pilotId=Novisad&callback=https%3A%2F%2Fcityimprovementnetapi.azurewebsites.net%2F&lang=SR";
            webView = new WebView
            {
                Source = new UrlWebViewSource
                {
                    Url = url,
                },
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            webView.Navigating += WebView_Navigating;
            webView.Navigated += WebView_Navigated;
            Content = new StackLayout
            {
                Children =
                {
                    webView
                }
            };
            dialog.Hide();
        }

        private void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            dialog.Hide();
        }

        private async void WebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            App.Current.MainPage = new MyMasterDetailPage();  
        }
    }
}