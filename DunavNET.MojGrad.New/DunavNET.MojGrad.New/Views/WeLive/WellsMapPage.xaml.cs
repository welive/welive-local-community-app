﻿using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DunavNET.MojGrad.New.Views.WeLive
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WellsMapPage : ContentPage
    {
        public WellsMapPage()
        {
            InitializeComponent();
            Title = LocalizationResource.Wells;
            var viewModel = new WellsMapViewModel(Navigation, MyMap);
        }
    }
}