﻿using DunavNET.MojGrad.New.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DunavNET.MojGrad.New.Views.WeLive
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LocalCommunityPage : ContentPage
	{
		public LocalCommunityPage ()
		{
			InitializeComponent();

            var viewModel = new LocalCommunityViewModel(Navigation, MyMap);

            topicPicker.SelectedIndexChanged += delegate
            {
                viewModel.RefreshPinsCommand.Execute(null);
            };

            communityPicker.SelectedIndexChanged += delegate
            {
                viewModel.RefreshPinsCommand.Execute(null);
            };

            BindingContext = viewModel;
		}
	}
}