﻿using Acr.UserDialogs;
using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Models;
using DunavNET.MojGrad.New.Services;
using DunavNET.MojGrad.New.Tools;
using DunavNET.MojGrad.New.Views.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Views.Authentication
{
    public class ProviderWebViewPage : ContentPage
    {
        private string mProvider;
        private IProgressDialog mDialog;
        private string mUrl;
        private WebView mWebView;
        private bool mIsErrorShown;

        public ProviderWebViewPage(string provider)
        {
            Title = LocalizationResource.Title;

            mProvider = provider;

            ShowLoading();
        }


        protected override async void OnAppearing()
        {
            mIsErrorShown = false;
            List<AuthenticatonModel> providers = await AuthenticationService.GetAuthProviders();
            if (providers.Any())
            {
                mUrl = AuthenticationConstants.BASE_API_URL + providers.FirstOrDefault(x => x.Name.Equals(mProvider)).Url;

                mWebView = new WebView
                {
                    Source = new UrlWebViewSource
                    {
                        Url = mUrl,
                    },
                    VerticalOptions = LayoutOptions.FillAndExpand
                };

                mWebView.Navigating += WebView_Navigating;
                mWebView.Navigated += WebView_Navigated;
                // Build the page.
                this.Content = new StackLayout
                {
                    Children =
                {
                    mWebView
                }
                };
            }
            else
            {
                await Navigation.PopAsync();
            }
            HideLoading();
        }

        private void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            HideLoading();

        }

        private async void WebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            ShowLoading();

            string token = AuthenticationService.ExtractTokenFromUrl(e.Url);

            if (!string.IsNullOrEmpty(token))
            {

                UserInfoModel model = await AuthenticationService.GetUserInfo(token);
                HideLoading();
                if (!model.HasRegistered)
                {
                    mWebView.IsVisible = false;
                    await Navigation.PopAsync();
                }
                else
                {
                    LocalData.SaveTokenLocal(token);
                    string email = model.Email;
                    if (string.IsNullOrEmpty(email))
                    {
                        email = (await AuthenticationService.GetUserInfo(token)).UserMail;
                    }
                    LocalData.SaveEmailLocal(model.Email);                       
                    App.Current.MainPage = new MyMasterDetailPage();
                    
                }

            }
            else if (e.Url.Contains("error="))
            {
                if (!mIsErrorShown)
                {
                    mIsErrorShown = true;
                    HideLoading();

                    await DisplayAlert(LocalizationResource.Info, LocalizationResource.LoginError, LocalizationResource.Ok);
                    await Navigation.PopAsync();
                    
                }
            }

            
        }

        private void ShowLoading()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (mDialog == null)
                    mDialog = UserDialogs.Instance.Loading(LocalizationResource.Loading);
                mDialog.Show();
            });
        }

        private void HideLoading()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if(mDialog == null)
                    mDialog = UserDialogs.Instance.Loading(LocalizationResource.Loading);
                mDialog.Hide();
            });
        }
    }
}
