﻿using DunavNET.MojGrad.New.Models;
using DunavNET.MojGrad.New.Services;
using DunavNET.MojGrad.New.Tools;
using DunavNET.MojGrad.New.Views.Master;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DunavNET.MojGrad.New.Views.Authentication
{
    public partial class OAuthNativeFlowPage : ContentPage
    {
        Account account;
        AccountStore store;

        public OAuthNativeFlowPage()
        {
            InitializeComponent();

            store = AccountStore.Create();
            account = store.FindAccountsForService(XamarinAuth.AppName).FirstOrDefault();
        }

        void OnLoginClicked(object sender, EventArgs e)
        {
            string clientId = null;
            string redirectUri = null;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    clientId = XamarinAuth.iOSClientId;
                    redirectUri = XamarinAuth.iOSRedirectUrl;
                    break;

                case Device.Android:
                    clientId = XamarinAuth.AndroidClientId;
                    redirectUri = XamarinAuth.AndroidRedirectUrl;
                    break;
            }

            var authenticator = new OAuth2Authenticator(
                clientId,
                null,
                XamarinAuth.Scope,
                new Uri(XamarinAuth.AuthorizeUrl),
                new Uri(redirectUri),
                new Uri(XamarinAuth.AccessTokenUrl),
                null,
                true);

            authenticator.ClearCookiesBeforeLogin = true;
            authenticator.Completed += OnAuthCompleted;
            authenticator.Error += OnAuthError;

            AuthenticationState.Authenticator = authenticator;

            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(authenticator);

        }
        
        async void OnAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            User user = null;
            if (e.IsAuthenticated)
            {
                // If the user is authenticated, request their basic user data from Google
                // UserInfoUrl = https://www.googleapis.com/oauth2/v2/userinfo
                var request = new OAuth2Request("GET", new Uri(XamarinAuth.UserInfoUrl), null, e.Account);
                string token = null;
                e.Account.Properties.TryGetValue("access_token", out token);

                if (!string.IsNullOrEmpty(token))
                {
                    string accessToken = await AuthenticationService.GoogleLogin(token);

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        UserInfoModel model = await AuthenticationService.GetUserInfo(accessToken);
                       
                        LocalData.SaveTokenLocal(accessToken);
                        string email = model.Email;

                        LocalData.SaveEmailLocal(model.Email);
                       
                        App.Current.MainPage = new MyMasterDetailPage();

                        
                    }
                }
                //var response = await request.GetResponseAsync();
                //if (response != null)
                //{
                //    // Deserialize the data and store it in the account store
                //    // The users email address will be used to identify data in SimpleDB
                //    string userJson = await response.GetResponseTextAsync();
                //    user = JsonConvert.DeserializeObject<User>(userJson);
                //}

                if (account != null)
                {
                    store.Delete(account, XamarinAuth.AppName);
                }

                await store.SaveAsync(account = e.Account, XamarinAuth.AppName);
                await DisplayAlert("Email address", user.Email, "OK");
            }
        }

        void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            Debug.WriteLine("Authentication error: " + e.Message);
        }
    }
}
