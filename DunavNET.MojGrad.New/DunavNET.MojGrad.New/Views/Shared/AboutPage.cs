﻿using DunavNET.MojGrad.New.AppResources;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Views.Shared
{
	public class AboutPage : ContentPage
	{
		public AboutPage()
		{
			Title = LocalizationResource.Title;

			Padding = new Thickness(30, 20, 30, 0);

			Label title = new Label { FontSize = 25, HorizontalOptions = LayoutOptions.Center, Text = LocalizationResource.AboutApp, HeightRequest = 70 };

			//Label mg = new Label { FontSize = 15, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.AboutAppText, HorizontalTextAlignment = TextAlignment.Start };

            Label lc = new Label { FontSize = 15, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.AboutLocalCommunity, HorizontalTextAlignment = TextAlignment.Start };


            Content = new StackLayout
			{
				Children =
				{
					title,				
                    lc
				}
			};
		}


	}
}
