﻿using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Tools;
using DunavNET.MojGrad.New.Views.Authentication;
using DunavNET.MojGrad.New.Views.Master;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Views.Shared
{
    public class NoInternetPage : ContentPage
    {
        public NoInternetPage()
        {
            Title = LocalizationResource.Title;

            Padding = new Thickness(50, 20, 50, 0);

            Label title = new Label { FontSize = 25, HorizontalOptions = LayoutOptions.Center, Text = LocalizationResource.ProblemWithInternet, HeightRequest = 100 };

            Label text = new Label { FontSize = 17, HorizontalOptions = LayoutOptions.FillAndExpand, Text = LocalizationResource.ProblemWithInternetMsg, HeightRequest = 100, HorizontalTextAlignment = TextAlignment.Center };

            Button retry = new Button { HorizontalOptions = LayoutOptions.Center, Text = LocalizationResource.Retry, BackgroundColor = MyColors.ACCENT, TextColor = Color.White };

            retry.Clicked += Retry_Clicked;

            Content = new StackLayout
            {
                Children =
                {
                    title,
                    text,
                    retry
                }
            };
        }

        private async void Retry_Clicked(object sender, EventArgs e)
        {
            bool isConnected = CrossConnectivity.Current.IsConnected;

            if (!isConnected)
            {
                string title = LocalizationResource.ProblemWithInternet;
                string message = LocalizationResource.NoInternetConnection;

                await DisplayAlert(title, message, LocalizationResource.Ok);

            }
            else
            {
                string token = string.Empty;
                try
                {
                    token = LocalData.GetLocalToken();
                }
                catch
                {
                    token = string.Empty;
                }


                if (string.IsNullOrEmpty(token))
                {
                    LoginPage loginPage = new LoginPage();
                    App.Current.MainPage = new NavigationPage(loginPage);
                    NavigationPage.SetHasNavigationBar(loginPage, false);
                    App.Current.MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
                    App.Current.MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);


                }
                else
                {
                    App.Current.MainPage = new MyMasterDetailPage();
                }
            }


        }
    }
}
