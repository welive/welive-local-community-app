﻿using DunavNET.MojGrad.New.Models;
using DunavNET.MojGrad.New.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DunavNET.MojGrad.New.Services
{
    public class AuthenticationService
    {

        public static async Task<List<AuthenticatonModel>> GetAuthProviders()
        {
            string url = AuthenticationConstants.GET_LOGIN_PROVIDERS;

            List<AuthenticatonModel> providers = new List<AuthenticatonModel>();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                HttpResponseMessage result = await client.GetAsync(url);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string resultContent = await result.Content.ReadAsStringAsync();
                    providers = (List<AuthenticatonModel>)Newtonsoft.Json.JsonConvert.DeserializeObject(resultContent.ToString(), typeof(List<AuthenticatonModel>));
                }

            }

            return providers;
        }


        public static async Task<UserInfoModel> GetUserInfo(string token)
        {
            string url = AuthenticationConstants.GET_USER_INFO;

            UserInfoModel providers = new UserInfoModel();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + token);

                HttpResponseMessage result = await client.GetAsync(url);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string resultContent = await result.Content.ReadAsStringAsync();
                    providers = (UserInfoModel)Newtonsoft.Json.JsonConvert.DeserializeObject(resultContent.ToString(), typeof(UserInfoModel));
                    if (!providers.HasRegistered)
                    {
                        var emailData = new { Email = providers.UserMail };
                        HttpResponseMessage registerResult = await client.PostAsync(AuthenticationConstants.REGISTER_USER, new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(emailData), Encoding.UTF8, "application/json"));
                        if (registerResult.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            providers = await GetUserInfo(token);

                        }
                    }
                }

            }

            return providers;
        }

        public static async Task<string> GoogleLogin(string token)
        {
            string url = AuthenticationConstants.GOOGLE_LOGIN(token);
            string accessToken = null;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                HttpResponseMessage result = await client.GetAsync(url);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string resultContent = await result.Content.ReadAsStringAsync();
                    dynamic tokenResult = JsonConvert.DeserializeObject(resultContent);
                    accessToken = tokenResult.token;

                }
            }

            return accessToken;
        }

        public static string ExtractTokenFromUrl(string url)
        {
            if (url.Contains("access_token="))
            {
                string[] attributes = url.Split('&');

                string token = attributes.FirstOrDefault(s => s.Contains("access_token=")).Split('=')[1];

                return token;
            }

            return string.Empty;
        }       

    }
}
