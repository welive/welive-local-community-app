﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DunavNET.MojGrad.New.Models;
using DunavNET.MojGrad.New.Tools;
using System.Net.Http.Headers;
using System.Net;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Services
{
    public class DataService
    {
        private const int NOVI_SAD = 1;

        public static async Task<ObservableCollection<LocalCommunityModel>> GetLocalCommunities()
        {
            ObservableCollection<LocalCommunityModel> retVal = null;
            try
            {
                string url = "http://azuredev.dunavnet.eu/portalWeLiveLocalCommunity/Home/GetLocalCommunities";

                using (var client = new HttpClient())
                {
                    HttpResponseMessage result = await client.GetAsync(url);
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        string resultContent = result.Content.ReadAsStringAsync().Result;
                        retVal = (ObservableCollection<LocalCommunityModel>)Newtonsoft.Json.JsonConvert.DeserializeObject(resultContent, typeof(ObservableCollection<LocalCommunityModel>));
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retVal;
        }

        public static async Task<ObservableCollection<TopicModel>> GetLocalCommunityTopics()
        {
            ObservableCollection<TopicModel> retVal = null;
            try
            {
                string url = "http://azuredev.dunavnet.eu/portalWeLiveLocalCommunity/Home/GetTopics";
                using (var client = new HttpClient())
                {
                    HttpResponseMessage result = await client.GetAsync(url);
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        string resultContent = result.Content.ReadAsStringAsync().Result;
                        retVal = (ObservableCollection<TopicModel>)Newtonsoft.Json.JsonConvert.DeserializeObject(resultContent, typeof(ObservableCollection<TopicModel>));
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return retVal;
        }

        public static async Task<ObservableCollection<LocalCommunityPostModel>> GetCommunityPosts(long? Id = null)
        {
            ObservableCollection<LocalCommunityPostModel> retVal = null;
            try
            {
                string url = (Id.HasValue) ? "http://azuredev.dunavnet.eu/portalWeLiveLocalCommunity/Home/GetPosts?Id=" + Id.Value : "http://azuredev.dunavnet.eu/portalWeLiveLocalCommunity/Home/GetPosts";
                using (var client = new HttpClient())
                {
                    HttpResponseMessage result = await client.GetAsync(url);
                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        string resultContent = result.Content.ReadAsStringAsync().Result;
                        retVal = (ObservableCollection<LocalCommunityPostModel>)Newtonsoft.Json.JsonConvert.DeserializeObject(resultContent, typeof(ObservableCollection<LocalCommunityPostModel>));
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return retVal;
        }

        public static bool GetFlag()
        {
            var pred = Application.Current.Properties.Any(x => x.Key == "Flag");
            bool flag = pred && Application.Current.Properties["Flag"] != null ? (bool)Application.Current.Properties["Flag"] : false;

            return flag;
        }

        public static void SaveFlag()
        {
            Application.Current.Properties["Flag"] = true;
        }

        public static async Task<bool> PostLogWeLive(string msg, string type)
        {
            try
            {
                string url = "https://dev.welive.eu/dev/api/log/localcommunity";
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer 6899f058-58a5-4341-84ba-2f1165450df4");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string content = Newtonsoft.Json.JsonConvert.SerializeObject(
                    new
                    {
                        msg = msg,
                        appId = "localcommunity",
                        type = type,
                        timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                        custom_attr = new { appName = "localcommunity" }
                    });

                    using (var result = await client.PostAsync(url, new StringContent(content, Encoding.UTF8, "application/json")))
                    {
                        if (result.StatusCode == HttpStatusCode.OK) return true;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<ObservableCollection<WellDataModel>> GetWellDataAsync()
        {
            ObservableCollection<WellDataModel> retVal = null;
            using (var client = new HttpClient())
            {
                string url = "https://dev.welive.eu/visualcomposer/mashup/invoke/255";
                try
                {
                    var response = await client.PostAsync(url, new StringContent(""));
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        retVal = (ObservableCollection<WellDataModel>)Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(ObservableCollection<WellDataModel>));
                    }
                }
                catch (Exception ex)
                {
                    retVal = new ObservableCollection<WellDataModel>();
                }
            }
            return new ObservableCollection<WellDataModel>(retVal.Where(x => x.GeoX.HasValue && x.GeoY.HasValue));
        }
    }
}
