﻿using DunavNET.MojGrad.MobileApp;
using DunavNET.MojGrad.New.AppResources;
using DunavNET.MojGrad.New.Services;
using DunavNET.MojGrad.New.Tools;
using DunavNET.MojGrad.New.Views.Authentication;
using DunavNET.MojGrad.New.Views.Master;
using DunavNET.MojGrad.New.Views.Shared;
using DunavNET.MojGrad.New.Views.WeLive;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Globalization;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New
{
    public partial class App : Application
    {
        private static int _StartTime = 0;

        public App()
        {
            InitializeComponent();

            //Otkomentarisati za lokalizaciju
            CultureInfo ci = new CultureInfo("sr-Latn-RS");

            LocalizationResource.Culture = ci; // set the RESX for resource localization
            DependencyService.Get<ILocalize>().SetLocale(ci); // set the Thread for locale-aware methods
            if (!DataService.GetFlag())
            {
                MainPage = new NavigationPage(new TermsOfUsePage());
                return;
            }

            bool isConnected = CrossConnectivity.Current.IsConnected;            
            string token = LocalData.GetLocalToken();

            if (!isConnected)
            {
                MainPage = new NavigationPage(new NoInternetPage());
            }
            else
            {
                if (string.IsNullOrEmpty(token))
                {
                    LoginPage loginPage = new LoginPage();
                    MainPage = new NavigationPage(loginPage);
                    NavigationPage.SetHasNavigationBar(loginPage, false);
                    MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
                    MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                }
                else
                {                   
                    MainPage = new MyMasterDetailPage();
                }
            }            
        }

        protected override async void OnStart()
        {
            _StartTime++;

            await DataService.PostLogWeLive("app started", "AppStarted");

            bool isHostReachable = await CrossConnectivity.Current.IsRemoteReachable(AuthenticationConstants.BASE_API_URL);

            if (!isHostReachable)
            {
                MainPage = new NavigationPage(new NoInternetPage());
                MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
                MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
            }    

            CrossConnectivity.Current.ConnectivityChanged += async delegate
            {
                bool isConnected = CrossConnectivity.Current.IsConnected;
                isHostReachable = await CrossConnectivity.Current.IsRemoteReachable(AuthenticationConstants.BASE_API_URL);

                if (!isConnected || !isHostReachable)
                {
                    MainPage = new NavigationPage(new NoInternetPage());
                    MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
                    MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                }
            };

            CrossConnectivity.Current.ConnectivityTypeChanged += async delegate
            {
                bool isConnected = CrossConnectivity.Current.IsConnected;
                isHostReachable = await CrossConnectivity.Current.IsRemoteReachable(AuthenticationConstants.BASE_API_URL);

                if (!isConnected || !isHostReachable)
                {
                    MainPage = new NavigationPage(new NoInternetPage());
                    MainPage.SetValue(NavigationPage.BarBackgroundColorProperty, MyColors.PRIMARY);
                    MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
                }
            };
            
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }
        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
