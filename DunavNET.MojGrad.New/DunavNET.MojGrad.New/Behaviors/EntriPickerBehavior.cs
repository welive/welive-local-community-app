﻿using DunavNET.MojGrad.New.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DunavNET.MojGrad.New.Behaviors
{
    public class EntriPickerBehavior : Behavior<Entry>
    {
        private Entry mEntry;
        public static readonly BindableProperty PickerProperty = BindableProperty.CreateAttached("RelatedPicker", typeof(Picker), typeof(EntriPickerBehavior), null);

        public Picker RelatedPicker
        {
            get { return (Picker)GetValue(PickerProperty); }
            set { SetValue(PickerProperty, value); }
        }

        public static readonly BindableProperty PickerTypeProperty = BindableProperty.CreateAttached("PickerType", typeof(string), typeof(EntriPickerBehavior), null);
        public string PickerType
        {
            get { return (string)GetValue(PickerTypeProperty); }
            set { SetValue(PickerProperty, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.Focused += Bindable_Focused;
            RelatedPicker.Unfocused += RelatedPicker_Unfocused;
            mEntry = bindable;
        }



        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.Focused -= Bindable_Focused;
            RelatedPicker.Unfocused -= RelatedPicker_Unfocused;
        }

        private void RelatedPicker_Unfocused(object sender, FocusEventArgs e)
        {
            if (RelatedPicker.SelectedIndex != -1)
            {
                switch (PickerType)
                {
                    case "CATEGORY":
                        CategoryModel categoryModel = RelatedPicker.SelectedItem as CategoryModel;
                        mEntry.Text = categoryModel.Name;
                        break;
                    case "TOPIC":
                        TopicModel topicModel = RelatedPicker.SelectedItem as TopicModel;
                        mEntry.Text = topicModel.Name;
                        break;
                    case "COMMUNITY":
                        LocalCommunityModel communityModel = RelatedPicker.SelectedItem as LocalCommunityModel;
                        mEntry.Text = communityModel.Name;
                        break;
                    default:
                        CategoryModel categoryModel1 = RelatedPicker.SelectedItem as CategoryModel;
                        mEntry.Text = categoryModel1.Name;
                        break;

                }

            }
        }

        private void Bindable_Focused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                mEntry.Unfocus();
                if (RelatedPicker.IsFocused)
                    RelatedPicker.Unfocus();
                RelatedPicker.Focus();
            });
        }
    }
}
