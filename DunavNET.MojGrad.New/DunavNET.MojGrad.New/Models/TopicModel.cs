﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DunavNET.MojGrad.New.Models
{
    public class TopicModel
    {
        public long TopicId { get; set; }
        public string Name { get; set; }
    }

    public class CategoryModel
    {
        public long CategoryId { get; set; }
        public string Name { get; set; }
        public long TopicId { get; set; }
    }
}
