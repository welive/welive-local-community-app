﻿namespace DunavNET.MojGrad.New.Models
{
    public class LocalCommunityModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public decimal? Lat { get; set; }
        public decimal? Lng { get; set; }
    }

    public class LocalCommunityPostModel
    {
        public long Id { get; set; }
        public long TopicID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long CommunityId { get; set; }
        public Address Location { get; set; }
    }

    public class Address
    {
        public string AddressLine1 { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
