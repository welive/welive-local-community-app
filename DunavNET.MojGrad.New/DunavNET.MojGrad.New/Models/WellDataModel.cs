﻿using System;
using Newtonsoft.Json;
namespace DunavNET.MojGrad.New.Models
{
    public class WellDataModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("adresa")]
        public string Address { get; set; }
        [JsonProperty("naziv")]
        public string Name { get; set; }
        [JsonProperty("gps_x")]
        public double? GeoX { get; set; }
        [JsonProperty("gps_y")]
        public double? GeoY { get; set; }
        [JsonProperty("mesto")]
        public string Place { get; set; }
        [JsonProperty("opis")]
        public object Description { get; set; }
        [JsonProperty("tip")]
        public string Type { get; set; }
        [JsonProperty("merno_mesto_id")]
        public int StationId { get; set; }
        [JsonProperty("_joined_naziv")]
        public string JoinedName { get; set; }
        [JsonProperty("datum_uzorka")]
        public DateTime SampleDate { get; set; }
        [JsonProperty("ocena")]
        public int Rating { get; set; }
    }


    public class Rootobject
    {
        public Class1[] Property1 { get; set; }
    }

    public class Class1
    {
        public int id { get; set; }
        public string adresa { get; set; }
        public string naziv { get; set; }
        public float? gps_x { get; set; }
        public float? gps_y { get; set; }
        public string mesto { get; set; }
        public object opis { get; set; }
        public string tip { get; set; }
        public int merno_mesto_id { get; set; }
        public string _joined_naziv { get; set; }
        public DateTime datum_uzorka { get; set; }
        public int ocena { get; set; }
    }

}
