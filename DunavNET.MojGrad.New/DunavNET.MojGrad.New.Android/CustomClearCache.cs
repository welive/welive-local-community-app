using Android.Content;
using Android.Webkit;
using DunavNET.MojGrad.MobileApp.Droid;
using DunavNET.MojGrad.New.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(CustomClearCache))]
namespace DunavNET.MojGrad.MobileApp.Droid
{
	public class CustomClearCache : LogoutInterface
	{


		public void DoOnLogout()
		{
			//START - Za uklanjanje account naloga
			CookieSyncManager.CreateInstance(MainApplication.Context);
			CookieManager cookieManager = CookieManager.Instance;
			cookieManager.RemoveAllCookie();

			//END
		}
	}
}