
using Android.App;
using Android.OS;

namespace DunavNET.MojGrad.New.Droid
{
	[Activity(Label = "Local Community", Icon = "@drawable/icon", MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash")]
	public class SplashScreenActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here

			StartActivity(typeof(MainActivity));

		}
	}
}