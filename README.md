Local Community Application
========================

Android app that helps users keep up to date with the latest news from the local communities of the city of Novi Sad..

Requirements
-------------

The project has the following requirements that must be correctly installed and
configured:

*	Xamarin toolkit. Available at [https://www.xamarin.com/download)
* 	Visual Studio. Available at [https://www.visualstudio.com/downloads/)

Installation
------------
Clone the following repository somewhere.

	git clone git@bitbucket.org:welive/welive-local-community-app.git
	
Open solution in Visual Studio IDE, build project and deploy application on Android phone
	